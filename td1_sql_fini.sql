
------------------------------------------------------------------ 
-- select
------------------------------------------------------------------ 

select * from patient;

select id_patient, sexe from patient;

select count(*) from patient;

-- Q1 Afficher la table sÃ©jour 

select*from sejour


-- where
------------------------------------------------------------------ 

select *
from patient 
where sexe = 'M';

select *
from patient
where date_naissance > '1960-01-01';

-- Q2 SÃ©lectionner les patients de la ville 1

select*
from patient
where id_ville= '1'


-- Q3 Afficher les patients nÃ©s aprÃ¨s le 31/03/1986

select *
from patient
where date_naissance > '1986-03-31'

-- AND
------------------------------------------------------------------ 

select *
from patient
where date_naissance > '1960-01-01'
and sexe = 'F';

-- Q4 Afficher les sÃ©jours commencÃ©s aprÃ¨s le 01/02/2020 dans lâ€™hÃ´pital 1

select *
from sejour
where date_debut_sejour> '2020-02-01'
and id_hopital= '1'

-- IN
------------------------------------------------------------------ 

select * 
from patient
where id_ville in (1, 2);

-- Q5 Afficher les sÃ©jours des hÃ´pitaux 1 et 3

select *
from sejour
where id_hopital in (1, 3)

-- GROUP BY
------------------------------------------------------------------ 

select sexe, count(*)
from patient
group by sexe;

-- Q6 Compter le nombre de patient par ville

select id_ville, count(*)
from patient
group by id_ville

-- Q7 Compter le nombre de sÃ©jours par hopital
select id_hopital, count(*)
from sejour
group by id_hopital

-- INNER JOIN
------------------------------------------------------------------ 

select *
from patient p inner join ville v 
on p.id_ville = v.id_ville;

-- Q8 Modifier la requÃªte prÃ©cÃ©dente pour n'afficher que l'id_patient et le nom de la ville

select p.id_patient, v.ville
from patient p inner join ville v 
on p.id_ville = v.id_ville

-- Q9 Afficher, pour chaque sÃ©jour, les hÃ´ptiaux dans lesquels ils ont eu lieu

select s.id_sejour, h.hopital
from sejour s inner join hopital h
on s.id_hopital = h.id_hopital

-- Q10 Compter le nombre de patients par ville en affichant le NOM de la ville
Select v.ville , count(*) as id_patient
from patient p inner join ville v 
on p.id_ville = v.id_ville
group by v.ville


-- Q11 Compter le nombre de sÃ©jours par hÃ´pital en affichant le NOM de l'hÃ´pital
Select h.hopital, count(*) as id_sejour
From sejour s inner join hopital h
On s.id_hopital = h.id_hopital
Group by h.hopital

-- Q12 Compter le nombre de patients femme par ville en affichant le nom de la ville
Select v.ville, count(*) as id_patient
From patient p inner join ville v 
On p.id_ville = v.id_ville
Where p.sexe = 'F'
Group by v.ville, p.sexe
Order by p.sexe

-- Q13 Compter le nombre de sÃ©jours commenÃ§Ã©s aprÃ¨s le 01/02/2020 pour chaque hÃ´pital en affichant le nom de l'hÃ´pital

select h.hopital, count(*) date_debut_sejour
from sejour s inner join hopital h
on s.id_hopital = h.id_hopital
where s.date_debut_sejour>'2020-02-01'
group by h.hopital, s.date_debut_sejour
order by s.date_debut_sejour


-- insert
------------------------------------------------------------------ 

-- ExÃ©cuter la requÃªte et **interprÃ©ter** le rÃ©sultat :

INSERT INTO ville
(id_ville, ville)
VALUES(6, 'BÃ©thune');
On a inséré béthune dans la table ville en 6ème position

-- Q13 InsÃ©rer Loos dans la table ville


insert into ville 
(id_ville, ville)
values(7,'Loos')
-- update
------------------------------------------------------------------ 

-- ExÃ©cuter la requÃªte et **interprÃ©ter** le rÃ©sultat :

update ville set ville = 'Lens' where id_ville = 6;

-- Q14 Remplacer le libellÃ© de la ville numÃ©ro 7 par Douai

update ville set ville = 'Douai' where id_ville = 7

-- delete
------------------------------------------------------------------ 

-- ExÃ©cuter la requÃªte et **interprÃ©ter** le rÃ©sultat :

delete from ville where id_ville = 6;

-- Q15 supprimer la ville numÃ©ro 7
Delete from ville where id_ville = 7


